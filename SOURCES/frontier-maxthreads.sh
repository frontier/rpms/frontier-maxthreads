#!/bin/bash
# check the maximum threads used by servlets in tomcat and upload
#  the data to frontier.cern.ch
# Written by Weizhen Wang
# NOTE: this has nothing to do with awstats other than it shares the
#  upload mechanism using rsync and the rsync password-file

# Directory awstats program and password-file is installed in
AWSTATS_DIR=/etc/awstats

# Directory where data files are stored
DATA_DIR=/var/lib/frontier-maxthreads

# Directory tomcat installation is in
TOMCAT_DIR=/usr/share/frontier-tomcat/tomcat

# Tomcat logs are always at least symlinked under $TOMCAT_DIR
PATH_LOG=$TOMCAT_DIR/logs

if [ ! -d $PATH_LOG ]; then
    # exit silently if there are no tomcat logs on this system
    # The frontier-tomcat package is not required by this package
    exit 0
fi

AWSTATSCONF_FILE=$AWSTATS_DIR/awstatsconf

if [ -f $AWSTATSCONF_FILE ]; then
    source $AWSTATSCONF_FILE
else
    # exit silently because this is normal until the first time that
    #  run_awstats.sh runs and identifies this machine
    exit 0
fi

SITEPROJ="$AWSTATS_SITEPROJ"
THISHOST="$AWSTATS_NODENAME"

RUNDATE="`date +"%Y-%m-%d"`"

# The cronjobs for this executes every 5 minutes (because that's easy to
#  specify), but actually the work is done 10 seconds before the next 5
#  minute mark.  That's important because catalina.out is rotated at
#  midnight and this allows us to miss only a small amount of log entries
#  at just before midnight.
echo "Sleeping at `date`"
sleep 290

echo "Starting at `date`"

# first make sure a previous run isn't still happening
CRONLOCK="$DATA_DIR/.cronlock"
MYLOCK="$CRONLOCK.$$"
trap "rm -f $MYLOCK" 0
echo "$$" >$MYLOCK
if ln $MYLOCK $CRONLOCK; then
    :
else
    OTHERPID="`cat $CRONLOCK 2>/dev/null`"
    if [ -z "$OTHERPID" ]; then
	echo "Couldn't create lock but there's no PID in it" >&2
    elif kill -0 "$OTHERPID" 2>/dev/null; then
	echo "Other job $OTHERPID still running, exiting" >&2
	exit 2
    else
	echo "lock exists, but associated process $OTHERPID isn't running" >&2
    fi
    echo "Breaking lock"
    rm -f $CRONLOCK
    echo "$$" >$MYLOCK
    if ln $MYLOCK $CRONLOCK 2>/dev/null; then
        :
    else
        echo "Couldn't re-establish lock even after breaking it, exiting" >&2
        exit 2
    fi
fi
rm -f $MYLOCK
trap "rm -f $CRONLOCK" 0


logfile=$PATH_LOG/catalina.out
resultdir=$DATA_DIR/chkthread_$THISHOST
mkdir -p $resultdir
dailyresult=$resultdir/maxthreads.$THISHOST.$RUNDATE
parsestart=$DATA_DIR/.chkthread_parsestart.num

tp=`date +"%Y/%m/%d %H:%M:%S"`


let nextbyte="`stat -c %s $logfile` + 1"

if [ -e $parsestart ]; then
  cst=`cat $parsestart`
  if [ -z "$cst" ] || [ "$cst" -gt "$nextbyte" ]; then
    # previous end is greater than next byte, must have been truncated
    cst=1
  fi
else
  # first time -- start just after where it was when we checked
  cst=$nextbyte
fi


# Store the temporary file in the directory with space.  Use a fixed
#  name because there's no automated cleanup of temporary files.
tmp=$PATH_LOG/.maxthreadstmp

/sbin/runuser -s /bin/bash $FRONTIER_USER -c "tail -c +$cst $logfile > $tmp"
cex=`stat -c %s $tmp`
expr $cst + $cex > $parsestart

tst=`head -n 1 $tmp | awk '{print $2,$3,$4,$5}'`
ted=`tail -n 1 $tmp | awk '{print $2,$3,$4,$5}'`
echo "# [$tst] to [$ted]">>$dailyresult

instancedir="$TOMCAT_DIR/webapps/"

for instance in `ls $instancedir`
do

 th_threshold=`grep -v '^ *#' $instancedir$instance/WEB-INF/classes/config.properties | grep [mM]ax[tT]hreads | awk -F'[= ]' '{print $2}'`
 if [ ! $th_threshold ]; then
   th_threshold=100
 fi
 th_threshold=`expr $th_threshold \* 3 / 4`
 
 mth=`grep "^$instance " $tmp | grep stop | grep threads= | awk -F'[= ]' 'BEGIN{m=0}{if(m<$10){m=$10;}}END{print m}'`
 avms=`grep "^$instance " $tmp | grep stop | grep msecs= | awk -F'[= ]' 'BEGIN{m=0}{{m+=$12;}}END{if (NR>0) {print m/NR;} else{print 0}}'`
 dbms=`grep "^$instance " $tmp | grep "DB query finished" | grep msecs= | awk -F'[= ]' 'BEGIN{m=0}{{m+=$12;}}END{if (NR>0) {print m/NR;} else{print 0}}'`
 echo "$tp $instance maxthreads=$mth averagetime=$avms msec avedbquerytime=$dbms msec threadsthreshold=$th_threshold">>$dailyresult
done

rm $tmp

rsync_usr=$SITEPROJ
rsync_id=awstats-$rsync_usr
rsync_pwf=$AWSTATS_DIR/password-file

FRONTIER_USER="`stat -c %U $rsync_pwf`"

/sbin/runuser -s /bin/bash $FRONTIER_USER -c "rsync --timeout=30 --contimeout=10 --password-file=$rsync_pwf -rlptv $resultdir $rsync_usr@frontier.cern.ch::$rsync_id"
# keep a little over a month's worth here; a year is kept on frontier.cern.ch
find $resultdir -mtime +40 | xargs rm -f

echo "Finished at `date`"
echo
