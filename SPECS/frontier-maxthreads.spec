Summary: maxthreads client for Frontier Servers
Name: frontier-maxthreads
Version: 5
Release: 1
License: GPL
Group: System/Server
Source0: frontier-maxthreads.sh
Source1: frontier-maxthreads.cron
Source2: frontier-maxthreads.logrotate

Requires: frontier-awstats >= 6.9-3, frontier-tomcat

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
Collect thread information from frontier-tomcat logs

%install
mkdir -p $RPM_BUILD_ROOT/usr/sbin
mkdir -p $RPM_BUILD_ROOT/etc/cron.d
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d
mkdir -p $RPM_BUILD_ROOT/var/lib/%{name}
install -m 755 %{SOURCE0} $RPM_BUILD_ROOT/usr/sbin/%{name}.sh
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT/etc/cron.d/%{name}
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT/etc/logrotate.d/%{name}

%preun
if [ $1 -eq 0 ]; then
   # full uninstall
   rm -rf /var/lib/%{name}/* /var/lib/%{name}/.chkthread*
fi

%files
/usr/sbin/%{name}.sh
/etc/cron.d/%{name}
/etc/logrotate.d/%{name}
/var/lib/%{name}

%changelog
* Wed May 26 2021 Edita Kizinevic <edita.kizinevic@cern.ch> 5-1
- Change frontier-maxthreads.sh rsync options from the rsync -av to rsync
  -rlptv.

* Thu Jun 28 2018 Dave Dykstra <dwd@fnal.gov> 4-1
- Fix off-by-one error when looking for where to start next parsing.
  This caused cases of reparsing the whole log when there was no new
  activity in the past 5 minutes.

* Mon Feb 19 2018 Dave Dykstra <dwd@fnal.gov> 3-1
- Add rsync timeouts to prevent it from hanging indefinitely.
- Move temporary file to the same directory as catalina.out.

* Wed Jul 22 2015 Dave Dykstra <dwd@fnal.gov> 2-1
- Change to be exact match on instance name rather than exact only at the
  end.  In particular, 'atlr' was matching on 't0atlr' log entries.

* Sat Mar 21 2015 Dave Dykstra <dwd@fnal.gov> 1-3
- install the correct file for logrotate.d; it was accidentally installing
  a copy of the cron.d file into logrotate.d

* Thu Dec 19 2014 Dave Dykstra <dwd@fnal.gov> 1-2
- Remove generated chkthread data on final uninstall

* Thu Dec 11 2014 Dave Dykstra <dwd@fnal.gov> 1-1
- Initial packaging into its own rpm
